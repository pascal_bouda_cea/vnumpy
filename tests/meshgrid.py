import numpy as np
import vnumpy as vnp

a=(50,60,90),(52,63,92)
b=(3,15,30),(7,18,32)
c=(100,200,300),(105,205,305)
d=(10,20,30),(15,25,35)

xi=(a,b,c,d)
index,size=vnp.meshgrid_index(*xi)

vm={}
for r in np.arange(len(xi[0][0])):
    vm[r]=np.meshgrid(*[np.arange(xi[i][0][r],xi[i][1][r]) for i in np.arange(len(xi))],indexing='ij')    
    vm[r]=tuple([x.flatten() for x in vm[r]])
    
# rr=[np.meshgrid(*(range(x[])),indexing='ij')]
# rr = [ r.flatten() for r in rr ]

# print(np.allclose(jj[:rr[1].size],rr[1]))
# print(np.allclose(kk[:rr[2].size],rr[2]))